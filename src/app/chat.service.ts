import { Injectable } from '@angular/core';
import { StompService } from 'ng2-stomp-service';

@Injectable()
export class ChatService {

  private subscription : any;
  constructor(stomp: StompService) {

    stomp.configure({
      host: 'http://localhost:8080/gs-guide-websocket',
      debug: true,
      queue: {'init': false}
    });

    stomp.startConnect().then(() => {
      stomp.done('init');
      console.log('connected');
    });
  }
}
